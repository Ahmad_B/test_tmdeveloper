<?php 
class Igd extends CI_controller{

    function index(){
        echo "Pelayanan IGD Rumah Sakit<hr>";
        echo "<ul>
        <li>".anchor('Igd/list_pasien',' List Pasien')."</li>
        <li>".anchor('Igd/pendaftaran',' Pendaftaran Layanan IGD')."</li>
        </ul>";
    }

    function pendaftaran(){        
        $this->load->model('model_layananigd');
        $data['layanan'] = $this->model_layananigd->list_layanan()->result();
        $this->load->view('pendaftaran',$data);
    }
    function input_simpan(){
        $databarang = array(
            'kode_barang' => $this->input->post('kode_barang'),
            'nama_barang' => $this->input->post('nama_barang'),
            'harga' => $this->input->post('harga_barang'));
        $this->db->insert('barang',$databarang);
        redirect('barang');
    }

    function list_pasien(){
        $this->load->model('model_pasien');
        $data['pasien'] = $this->model_pasien->list_pasien()->result();
        $this->load->view('list_pasien',$data);        
    }
    function input_pasien(){
        $this->load->model('model_pasien');
        $data['pasien'] = $this->model_pasien->list_pasien()->result();
        $this->load->view('input_pasien',$data);
    }
    function pasien_simpan(){
        $datapasien = array(
            'no_rm' => $this->input->post('no_rm'),
            'nama_pasien' => $this->input->post('nama_pasien'),
            'alamat' => $this->input->post('alamat'),
            'golongan' => $this->input->post('golongan'));
        $this->db->insert('pasien',$datapasien);
        redirect('Igd/list_pasien');
    }
    function layananIGD_simpan(){
        $datapasien = array(
            'no_rm' => $this->input->post('no_rm'),
            'nama_pasien' => $this->input->post('nama_pasien'),
            'alamat' => $this->input->post('alamat'),
            'golongan' => $this->input->post('golongan'));
        $this->db->insert('pasien',$datapasien);
        redirect('Igd/list_pasien');
    }
}


?>