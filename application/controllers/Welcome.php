<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$data['contents'] = 'admin/dashboard';		
		$data['title'] = 'Dashboard';
		$this->load->view('assets/index', $data);
	}
	public function daftar_pasien()
	{
		$data['contents'] = 'admin/daftar_pasien';
		$data['title'] = 'Daftar Pasien';
		$data['title2'] = 'Pasien';
		$this->load->model('model_pasien');
		$data['pasien'] = $this->model_pasien->list_pasien()->result();
		$this->load->view('assets/index', $data);
	}
}
