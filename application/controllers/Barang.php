<?php 
class Barang extends CI_controller{

    function index(){
        $this->load->model('model_barang');
        $judul = 'Daftar Barang';
        $data['judul'] =  $judul;
        $data['barang'] = $this->model_barang->list_barang()->result();
        $this->load->view('list_barang',$data);
    }

    function input(){
        $this->load->view('input_barang');
    }
    function input_simpan(){
        $databarang = array(
            'kode_barang' => $this->input->post('kode_barang'),
            'nama_barang' => $this->input->post('nama_barang'),
            'harga' => $this->input->post('harga_barang'));
        $this->db->insert('barang',$databarang);
        redirect('barang');
    }
    function pasien_simpan(){
        $databarang = array(
            'kode_barang' => $this->input->post('kode_barang'),
            'nama_barang' => $this->input->post('nama_barang'),
            'harga' => $this->input->post('harga_barang'));
        $this->db->insert('barang',$databarang);
        redirect('barang');
    }

    function edit(){
        echo "ini Edit";
    }
}


?>