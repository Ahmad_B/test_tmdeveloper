<?php echo form_open('igd/pasien_simpan'); ?>
<table>
    <?php 
        $y = date("y");
        $lastKey = array_values(array_slice($pasien, -1))[0];
        /*echo substr($lastKey->no_rm,-6);
        echo "<hr>";*/
        $no = ($y*1000000)+(substr($lastKey->no_rm,-6)+1);
    ?>
    <tr><td>Nomor Rekam Medis</td><td>:</td><td><?php echo form_input('no_rm',$no)?></td></tr>
    <tr><td>Nama Pasien</td><td>:</td><td><?php echo form_input('nama_pasien','',array('placeholder'=>'Nama Pasien'))?></td></tr>
    <tr><td>Alamat</td><td>:</td><td><?php echo form_input('alamat','',array('placeholder'=>'Alamat'))?></td></tr>
    <tr><td>Golongan</td><td>:</td><td><?php echo form_input('golongan','',array('placeholder'=>'Golongan'))?></td></tr>
    <tr><td colspan="2"><?php echo form_submit('submit','Simpan Data')?><?php echo anchor('igd',' -> kembali ke menu awal'); ?></td></tr>
</table>
<?php echo form_close(); ?>