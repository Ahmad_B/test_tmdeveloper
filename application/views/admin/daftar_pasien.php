<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Cari Pasien : </h3> <input type="text" name="firstname" value="Mickey">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No. Rekam Medis</th>
                  <th>Nama Pasien</th>
                  <th>Golongan</th>
                </tr>
                </thead>
                <?php      
                    foreach ($pasien as $p){
                        echo "
                        <tr>
                        <td>".$p->no_rm."</td>
                        <td>".$p->nama_pasien."</td>
                        <td>".$p->golongan."</td>
                        </tr>";
                    }
                ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
    </div>
</section>
